Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: jaeger-lib
Source: https://github.com/jaegertracing/jaeger-lib

Files: *
Copyright:
 2017-2018 Uber Technologies, Inc.
 2017-2020 The Jaeger Authors
License: Apache-2.0

Files: DCO
Copyright: 2004, 2006 The Linux Foundation and its contributors
License: DCO-1.1
Comment: CONTRIBUTING.md also contains an embedded copy of the DCO

Files: debian/*
Copyright: 2023 Mathias Gibbens
License: Apache-2.0
Comment: Debian packaging is licensed under the same terms as upstream

License: Apache-2.0
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 .
 http://www.apache.org/licenses/LICENSE-2.0
 .
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
Comment:
 On Debian systems, the complete text of the Apache version 2.0 license
 can be found in "/usr/share/common-licenses/Apache-2.0".

License: DCO-1.1
 Developer Certificate of Origin
 Version 1.1
 .
 Copyright (C) 2004, 2006 The Linux Foundation and its contributors.
 660 York Street, Suite 102,
 San Francisco, CA 94110 USA
 .
 Everyone is permitted to copy and distribute verbatim copies of this
 license document, but changing it is not allowed.
 .
 Developer's Certificate of Origin 1.1
 .
 By making a contribution to this project, I certify that:
 .
 (a) The contribution was created in whole or in part by me and I
     have the right to submit it under the open source license
     indicated in the file; or
 .
 (b) The contribution is based upon previous work that, to the best
     of my knowledge, is covered under an appropriate open source
     license and I have the right under that license to submit that
     work with modifications, whether created in whole or in part
     by me, under the same open source license (unless I am
     permitted to submit under a different license), as indicated
     in the file; or
 .
 (c) The contribution was provided directly to me by some other
     person who certified (a), (b) or (c) and I have not modified
     it.
 .
 (d) I understand and agree that this project and the contribution
     are public and that a record of the contribution (including all
     personal information I submit with it, including my sign-off) is
     maintained indefinitely and may be redistributed consistent with
     this project or the open source license(s) involved.
